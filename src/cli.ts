import { CLI, Shim } from 'clime';
import { join } from 'path';

export type CmdDir = string | { label: string, path: string, }

export interface CliAndShim {
  cli: CLI;
  shim: Shim;
}

export class CommandLineApplication {

  private cliShim?: CliAndShim;
  private commandPaths: CmdDir[] = [
    {
      label: 'built-in',
      path: join(__dirname, 'commands'),
    }
  ];

  public get cli(): CLI | undefined {
    return this.cliShim ? this.cliShim.cli : undefined;
  }

  public get shim(): Shim | undefined {
    return this.cliShim ? this.cliShim.shim : undefined;
  }

  public addCommandDir(cmd: CmdDir): CommandLineApplication {
    this.commandPaths.push(cmd);
    return this;
  }

  public invoke(args?: any | any[]): void {
    const cliInstance = new CLI('docker-manifest', this.commandPaths);
    const shimInstance = new Shim(cliInstance);
    this.cliShim = {
      cli: cliInstance,
      shim: shimInstance,
    };
    shimInstance.execute(args);
  }

}

const cliApp = new CommandLineApplication();

export default cliApp;
