import { Command, command, metadata } from 'clime';
import { version } from '../../package.json';

@command({
  description: 'Prints current version information.'
})
export default class extends Command {
  @metadata
  public execute(): string {
    return `docker-manifest v${version}`;
  }
}
