import { Command, command, Options, option, metadata } from "clime";
import { ManifestHandler } from "../lib";
import * as path from 'path';
import { TaskRunner } from "../lib/runners/task-runner";
import { buildTasksFromManifest } from "../lib/task-executor";
import { enableConsoleOutput } from "../lib/utils/logger";

const manifestHandler = new ManifestHandler();

export class BuildOptions extends Options {
  @option({
    name: 'manifest',
    flag: 'm',
    description: 'Specify the manifest.yml to use for building.',
    required: true,
    default: 'manifest.yml'
  })
  manifest: string = '';

  @option({
    name: 'debug',
    flag: 'd',
    description: 'Enable debug output',
    required: false,
    default: false,
    toggle: true,
  })
  debug: boolean = false;

  public get manifestFileLocation(): string {
    if (this.manifest.trim().length <= 0) {
      throw new Error('Manifest file is required but not specified!');
    }
    return this.manifest.trim();
  }

  public get isDebugOutputEnabled(): boolean {
    return this.debug;
  }

}

@command({
  description: 'Build container images from a specified manifest.yml'
})
export default class extends Command {

  @metadata
  public async execute(options: BuildOptions): Promise<void> {

    if(options.debug) {
      enableConsoleOutput(true);
    }

    const manifestLocation = path.resolve(process.cwd(), options.manifest);

    const targetManifest = await manifestHandler.resolveAndLoadManifest({
      manifest: manifestLocation,
      mergeDefaults: true,
    });

    if(!targetManifest) {
      throw new Error('Unable to resolve manifest! Expected Location: ' + manifestLocation);
    }

    const runner = new TaskRunner();
    runner.addTasks(...buildTasksFromManifest(manifestLocation, targetManifest));
    return runner.executeTasks();
  }

}
