export const brief = 'docker-manifest toolkit for your manifest.yml';
export const description = 'View, test and build manifest.yml files';

export const subcommands = [
  {
    name: 'build',
    alias: 'b',
    brief: 'Build docker images from manifest.yml via docker-manifest',
  },
  {
    name: 'version',
    alias: 'v',
    brief: 'Prints current version information',
  }
];

