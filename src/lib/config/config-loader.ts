import cosmiconfig from 'cosmiconfig';
import { createLogger } from '../utils';

const logger = createLogger('ConfigLoader');

export async function loadConfig<T>(): Promise<T | undefined> {
  const explorer = cosmiconfig('docker-manifest');
  const result = await explorer.searchSync();
  if (!result) {
    logger.info('Explorer: Unable to find configuration file ');
    return undefined;
  }
  if(result.isEmpty || !result.config) {
    logger.info('Explorer: Found empty configuration ');
    return undefined;
  }
  logger.info('Explorer: Found configuration');
  return result.config as T;
}
