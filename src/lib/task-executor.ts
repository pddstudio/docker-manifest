import { ListrTask } from 'listr';
import { TaskRunner } from './runners/task-runner';
import { ManifestSpec } from './models';
import { buildTaskStackFromManifest } from './tasks/docker-tasks';

export async function runTasks(...tasks: ListrTask[]) {
  const taskRunner = new TaskRunner();
  taskRunner.addTasks(...tasks);
  return taskRunner.executeTasks();
}

export function buildTasksFromManifest(manifestLocation: string, manifest: ManifestSpec): ListrTask[] {
  const tasks: ListrTask[] = [];
  manifest.images.map(image => {
    const tmpTasks = buildTaskStackFromManifest({ manifestFile: manifestLocation, build: image });
    tasks.push(...tmpTasks);
  });
  return tasks;
}
