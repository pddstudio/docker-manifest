import {default as Listr, ListrTask, ListrOptions } from 'listr';
import { UpdaterRenderer } from 'listr-update-renderer';

interface ExtendedListrOptions extends ListrOptions {
  collapse?: boolean;
  showSubtasks?: boolean;
}

const listrOptions: ExtendedListrOptions = {
  renderer: UpdaterRenderer,
  concurrent: false,
  collapse: true,
  showSubtasks: true
}

export class TaskRunner {

  private readonly tasks: ListrTask[] = [];

  public addTasks(...task: ListrTask[]) {
    this.tasks.push(...task);
  }

  public async executeTasks(): Promise<void> {
    const runner = new Listr(this.tasks, listrOptions);
    return runner.run();
  }

}
