import * as shell from 'shelljs';
import { ImageSpec, DockerImage } from '../models';
import { logger } from '../utils';
import * as path from 'path';

type BuildTask = ImageSpec;

interface BuildOptions {
  manifestFile: string;
  build: BuildTask;
}

interface DockerBuildOptions {
  manifestFileLocation: string;
  contextDir?: string;
  useCache?: boolean;
  buildArgs?: any[];
  labels?: any[];
}

export async function runBuild(options: BuildOptions) {
  const { build, manifestFile } = options;
  // mute shell output during execution
  const shellStateOrig = shell.config.silent;
  shell.config.silent = true;

  const pullCmd = fmtImage(build.base_image);
  const pullOutput = await pullDockerImage(pullCmd);
  if (pullOutput.code !== 0) {
    logger.error('Non-zero exit code for docker pull command returned!');
    logger.error(pullOutput.stderr);
    // restore original config
    shell.config.silent = shellStateOrig;
    return;
  }

  logger.info(`Docker Image pulled for ${pullCmd}!`);

  if (build.build && build.build.required) {
    // docker build is required
    const buildOptions: DockerBuildOptions = {
      manifestFileLocation: manifestFile,
      contextDir: build.build.container_dir,
    };
    const buildOutput = await buildDockerImage(build.dest_image, buildOptions);

    if (buildOutput.code !== 0) {
      logger.error('Building docker image failed:');
      logger.error(buildOutput.stderr);
      // reset shell
      shell.config.silent = shellStateOrig;
      return;
    }
  }

  const tagOutput = await tagDockerImage(
    fmtImage(build.base_image),
    fmtImage(build.dest_image),
  );

  if (tagOutput.code !== 0) {
    logger.warn('docker tag command returned exit code: ' + tagOutput.code);
    logger.warn(tagOutput.stderr);
  }

  if (build.build && build.build.publish) {
    // docker push to registry
    logger.info('Pushing image to specified registry...');
    const publishOutput = await pushDockerImage(fmtImage(build.dest_image));

    if (publishOutput.code !== 0) {
      logger.error(
        'Publishing failed with non-zero exit code: ' + publishOutput.code,
      );
      logger.error(publishOutput.stderr);
    } else {
      logger.info('Image pushed succeeded!');
      logger.info('Target was: ' + fmtImage(build.dest_image));
    }
  }

  // restore original shell config
  shell.config.silent = shellStateOrig;
  logger.info('runBuild(): Finished processing for this docker image!');
}

const pullDockerImage = async (image: string) => {
  return await shell.exec(`docker pull ${image}`);
};

const tagDockerImage = async (origImage: string, image: string) => {
  return await shell.exec(`docker tag ${origImage} ${image}`);
};

const pushDockerImage = async (image: string) => {
  return await shell.exec(`docker push ${image}`);
};

const buildDockerImage = async (
  image: DockerImage,
  options: DockerBuildOptions,
) => {
  const buildDir = path.dirname(options.manifestFileLocation);
  const execData: string[] = ['pushd', buildDir, '&&', 'docker build'];

  if (options.useCache !== undefined && !options.useCache) {
    execData.push('--no-cache');
  }

  execData.push('--tag');
  execData.push(fmtImage(image));

  if (options.contextDir && options.contextDir.trim().length > 0) {
    const ctxDir = options.contextDir.trim();
    const dockerFile = path.join(ctxDir, 'Dockerfile');
    execData.push('-f', dockerFile, ctxDir);
  } else {
    execData.push('.');
  }
  execData.push('&&', 'popd');

  const shellCmd = execData.join(' ');
  logger.info(`Building docker image with command:
  => ${shellCmd}`);

  return await shell.exec(shellCmd);
};

const fmtImage = (image: DockerImage) => {
  const hasPrefix = image.image.trim().includes('/');
  const hasCustomRegistry = image.registry && image.registry.trim().length > 0;

  const formattedImageName = hasPrefix ? image.image : `library/${image.image}`;
  const formattedRegistry = hasCustomRegistry ? `${image.registry}/` : '';

  return `${formattedRegistry}${formattedImageName}:${image.tag}`;
};
