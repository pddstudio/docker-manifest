import { ManifestSpec } from "./models";
import * as path from 'path';
import yaml from 'js-yaml';
import beautify from 'json-beautify';
import { mergeManifestDefaults, logger } from './utils';
import * as fs from 'fs';
import { runBuild } from './runners/run-shell';

export interface HandlerOptions {
  manifest: string,
  mergeDefaults: boolean,
}

export class ManifestHandler {

  public constructor() {
    //
  }

  public async resolveAndLoadManifest(options: HandlerOptions): Promise<ManifestSpec | undefined> {
    const manifestLocation = path.resolve(process.cwd(), options.manifest);
    let finalManifest;
    try {
      const mainfest: ManifestSpec = await yaml.safeLoad(
        await fs.readFileSync(manifestLocation, 'utf-8'),
      );
      logger.info(`Manifest resolved:
      ${beautify(mainfest, null, 2, 100)}`);

      if (options.mergeDefaults) {
        finalManifest = await mergeManifestDefaults(mainfest);
      } else {
        finalManifest = mainfest;
      }

      logger.info('Mergin defaults....');
      logger.info(`Final Manifest:
      ${beautify(finalManifest, null, 2, 100)}`);
    } catch (error) {
      logger.error(error);
      return undefined;
    }
    return finalManifest;
  }

  public async buildImagesFromManifest(manifestLocation: string, manifest: ManifestSpec): Promise<void> {
    if (!manifest) {
      logger.error('Manifest cannot be undefined!');
      return;
    }

    logger.info(`Building images for projet: ${manifest.name}`);
    for (const image of manifest.images) {
      await runBuild({ manifestFile: manifestLocation, build: image });
    }

    logger.info('Finished processing manfiest.yml!');
  }
}
