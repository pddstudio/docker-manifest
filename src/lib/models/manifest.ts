export interface ManifestSpec {
  name: string;
  description?: string;
  author?: string;
  homepage?: string;
  repository?: string;
  registry?: string;
  images: ImageSpec[];
}

export interface ImageSpec {
  base_image: DockerImage;
  dest_image: DockerImage;
  build?: BuildSpec;
}

export interface BuildSpec {
  required?: boolean;
  publish?: boolean;
  container_dir?: string;
}

export interface DockerImage {
  registry?: string;
  image: string;
  tag?: string;
}
