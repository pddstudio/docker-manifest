import { ListrTask } from 'listr';
import { ImageSpec, DockerImage, BuildSpec } from '../models';
import * as path from 'path';
import { formatImageName } from '../utils/formatImageName';
import execa from 'execa';

type BuildTask = ImageSpec;

interface BuildOptions {
  manifestFile: string;
  build: BuildTask;
}

interface DockerBuildOptions {
  manifestFileLocation: string;
  contextDir?: string;
  useCache?: boolean;
  buildArgs?: any[];
  labels?: any[];
}

export function buildTaskStackFromManifest(options: BuildOptions): ListrTask[] {
  const { manifestFile, build } = options;
  const pullTask = dockerPull(build);
  const tagTask = dockerTag(build);
  const buildTask = dockerBuild(manifestFile, build.dest_image, build.build)
  const pushTask = dockerPush(build);
  return [ pullTask, tagTask, buildTask, pushTask];
}

function dockerPull(imageSpec: ImageSpec): ListrTask {
  const formattedImageName = formatImageName(imageSpec.base_image);
  return {
    title: `Pulling docker image for ${imageSpec.base_image.image}`,
    task: () => execa('docker', ['pull', formattedImageName])
  }
}

function dockerTag(imageSpec: ImageSpec): ListrTask {
  const image = formatImageName(imageSpec.dest_image);
  const origImage = formatImageName(imageSpec.base_image);
  return {
    title: `Tagging Docker Image ${image}`,
    task: () => execa('docker', ['tag', origImage, image])
  }
}

function dockerPush(imageSpec: ImageSpec): ListrTask {
  const image = formatImageName(imageSpec.dest_image);
  return {
    title: `Publishing docker image to ${image}`,
    task: () => execa('docker', ['push', image])
  }
}

function dockerBuild(manifestFile: string, dockerImage: DockerImage, buildSpec?: BuildSpec): ListrTask {
  const buildOptions: DockerBuildOptions = {
    manifestFileLocation: manifestFile,
    contextDir: buildSpec ? buildSpec.container_dir : '',
  };
  const args = formatBuildParams(dockerImage, buildOptions);
  return {
    title: `Building docker image`,
    skip: () => {
      if(!buildSpec) {
        return 'No build instructions specified'
      }
      if(!(buildSpec.required)) {
        return 'Build required option is set to false'
      }
    },
    task: () => execa.shell(args.join(' ')),
  }
}

const formatBuildParams = (
  image: DockerImage,
  options: DockerBuildOptions,
) => {
  const buildDir = path.dirname(options.manifestFileLocation);
  const execData: string[] = ['pushd', buildDir, '&&', 'docker', 'build'];

  if (options.useCache !== undefined && !options.useCache) {
    execData.push('--no-cache');
  }

  execData.push('--tag');
  execData.push(formatImageName(image));

  if (options.contextDir && options.contextDir.trim().length > 0) {
    const ctxDir = options.contextDir.trim();
    const dockerFile = path.join(ctxDir, 'Dockerfile');
    execData.push('-f', dockerFile, ctxDir);
  } else {
    execData.push('.');
  }
  execData.push('&&', 'popd');

  return execData;
};
