import { Logger } from 'winston';
// tslint:disable-next-line:no-duplicate-imports
import * as winston from 'winston';

const { combine, colorize, label, prettyPrint, printf } = winston.format;

const defaultTransports: any[] = [new winston.transports.Console()];
let defaultLevel = 'error';

const loggerFormat = printf(info => {
  return `[${info.label}] ${info.level}: ${info.message}`;
});

const logger: Logger = winston.createLogger({
  level: defaultLevel,
  transports: defaultTransports,
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.simple(),
  ),
});

export function enableConsoleOutput(enabled: boolean = false, logLevel: string = 'debug') {
  defaultLevel = logLevel;
  if(!enabled) {
    defaultLevel = 'error'
  }
  logger.level = defaultLevel;
}

function setupLogger(prefix: string): Logger {
  return winston.createLogger({
    exitOnError: false,
    level: defaultLevel,
    transports: defaultTransports,
    format: combine(
      colorize(),
      label({ label: prefix }),
      prettyPrint(),
      loggerFormat,
    ),
  });
}

export const createLogger = (prefix: string) => setupLogger(prefix);

export default logger;
