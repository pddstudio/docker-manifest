import { default as loggerInstance } from './logger';
import { Logger } from 'winston';

export const logger: Logger = loggerInstance;
export { createLogger } from './logger';

export { mergeManifestDefaults } from './mergeManifestDefaults';
