import { DockerImage } from '../models';

export const formatImageName = (image: DockerImage) => {
  const hasPrefix = image.image.trim().includes('/');
  const hasCustomRegistry = image.registry && image.registry.trim().length > 0;

  const formattedImageName = hasPrefix ? image.image : `library/${image.image}`;
  const formattedRegistry = hasCustomRegistry ? `${image.registry}/` : '';

  return `${formattedRegistry}${formattedImageName}:${image.tag}`;
};
