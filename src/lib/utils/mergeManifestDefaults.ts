import * as _ from 'lodash';
import { ManifestSpec, DockerImage, BuildSpec, ImageSpec } from '../models';

const dockerImageDefaults: DockerImage = {
  registry: '',
  tag: 'latest',
  image: '',
};

const buildSpecDefaults: BuildSpec = {
  required: false,
  publish: false,
};

const applyImageSpecDefaults = (imageSpec: ImageSpec) => {
  const resultBuildSpec = _.assign({}, buildSpecDefaults, imageSpec.build);
  const resultDockerBase = _.assign(
    {},
    dockerImageDefaults,
    imageSpec.base_image,
  );
  const resultDockerDest = _.assign(
    {},
    dockerImageDefaults,
    imageSpec.dest_image,
  );
  imageSpec.base_image = resultDockerBase;
  imageSpec.dest_image = resultDockerDest;
  imageSpec.build = resultBuildSpec;
  return imageSpec;
};

export const mergeManifestDefaults = (manifest: ManifestSpec) => {
  const manifestClone = _.cloneDeep(manifest);
  manifestClone.images.map(image => applyImageSpecDefaults(image));
  return manifestClone;
};
