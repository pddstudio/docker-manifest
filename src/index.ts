import cliApp, { CmdDir, CommandLineApplication } from './cli';

export function addCommandDir(cmd: CmdDir): CommandLineApplication {
  return cliApp.addCommandDir(cmd);
}

export default function exec(args?: any) {
  cliApp.invoke(args);
}
