# `docker-manifest`

> A small cli tool for `[automated]` handling of container stacks & multiple docker image publication.


![build](https://gitlab.com/pddstudio/docker-manifest/badges/develop/build.svg)


## Introduction

The main purpose *(but not limited to)* of this tool is to minimize the workload for setting up automated multi-image build jobs on CI environments.

### Other Possibilities

Basically this tool serves functionality for the following purposes:

- pulling docker images from remote registries
- building docker images
- re-tagging images from remote registries.
- pushing images to remote registries.

No matter which problems this tool is going to solve - everything is done with a single & simple `manifest.yml` file.

Everything can be used either local or on CI environments.

---

### Documentation

You can read the latest [documentation](https://pddstudio.gitlab.io/docker-manifest/) online.


## Installing `docker-manifest`

Make sure your environment satisfies the following requirements in order to install and use this tool:

- `A [node](https://nodejs.com) (v8 or greater) binary available in  your path`
- `A package manager for node, i.e[npm](https://npmjs.org/) or [yarn](yarnpkg.org)`
- `A valid docker[https://docker.com/] binary in your path`


### !!! ATTENTION !!!

This tool is **NOT** stable/ready for public usage yet!

You may *NOT* use this tool until reaching version `1.0.0`!

Use at your own risk!

---

Once all pre-requirements are met, you can install `docker-manifest` via:

```sh
# when using npm
npm i -g @pddstudio/docker-manifest

# when using yarn
yarn global add @pddstudio/docker-manifest

```

## Setup & Usage

To get started, create a `manifest.yml` file in your environment and provide *(at least)* a `name` for your project/stack and the basic actions for your image(s).

You can read the [Documentation](./ROADMAP.md) or the [Getting Started Guide](./ROADMAP.md) to get up and running.

For more inspiration you can dig through the collection of sample usages in the [examples](./examples/README.md) folder.

## Getting Help

```
// TODO
```

## Contributing

```
// TODO
```

## License

(c) 2018 - Patrick Jung <patrick.pddstudio@gmail.com> - [MIT License](./LICENSE)
