import { loadConfig, DockerManifestConfig } from '../../src/lib/config';

test('It should find a config file', async (done) => {
  const config: DockerManifestConfig = await loadConfig<DockerManifestConfig>();
  expect(config).toBeDefined();
  expect(config.manifestFile).toBeDefined();
  done();
});
