import * as shell from 'shelljs';
import { createLogger } from '../src/lib/utils';

const logger = createLogger('cli-usage-test');
const origShellSilent = shell.config.silent;

beforeAll(() => {
  jest.setTimeout(15000);
  shell.config.silent = true;
});

test('It should display the cli help for docker-manifest', async done => {
  let result;
  try {
    result = await shell.exec('docker-manifest --help');
  } catch (error) {
    logger.error('Code: ' + result.code + ' => ' + error);
  }

  logger.info(`
  Returned shell output:
  ${result.stdout.trim().length > 0 ? result.stdout : result.stderr}
  `);

  expect(result).toBeDefined();
  expect(result.code).toBe(0);

  done();
});

afterAll(() => {
  shell.config.silent = origShellSilent;
});
