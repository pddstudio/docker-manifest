# Installing

There are several ways to install `docker-manifest` on your target system.

!!! danger "Important information about installing versions before 1.0.0"
    It's **not** recommended to install or use `docker-manifest` prior to its initial stable `1.0.0` release.  
    It may cause damage to your system or lead to unwanted behavior.  
    There is no warranty. There is no support. (Except contributions related questions or bug reports)  
    _**Use at your own risk!**_  
    You have been warned...

---

### via `npm` or `yarn` (recommended)

The easiest and recommended way to install `docker-manifest` is using your favorite NodeJS package manager:

!!! note "Note: Selecting your package manager"
    By default the install command below contains the install instructions via `npm`.  
    You can view the instructions for `yarn` by simply switching to the yarn tab below.  
    

```bash tab="npm"

# installing via npm
npm install --global @pddstudio/docker-manifest

```

```bash tab="yarn"

# installing via yarn
yarn global add @pddstudio/docker-manifest

```

Once the installation completes you can call `docker-manifest` from your shell with any supported [CLI Command](./cli_commands.md)  


!!! info "Invoking `docker-manifest` immediately upon installation"
    Depending on your operating system & shell environment you may have to re-open your terminal for the `docker-manifest` command to be available.  
    If you don't can/want to close your current shell session you may instead re-source your shell's initial run command via `source ~/.YOUR_SHELL_RC` or `. ~/.YOUR_SHELL_RC`.  
    
---

### via `install.sh` Script (Linux/MacOS only)

> Not available yet. It's on the [roadmap](./roadmap) for the initial 1.0.0 stable release.

---

### via `brew` (MacOS only)

> Not available yet. It's on the [roadmap](./roadmap) for the initial 1.0.0 stable release.

---

## Alternatives

If you prefer to compile `docker-manifest` on your own instead of using one of the existing packages you can do this by following these simple steps:

```sh
# clone the repository
git clone https://gitlab.com/pddstudio/docker-manifest.git

# cd into the root of this project
cd /path/to/clone/docker-manifest

# install dependencies
yarn install

# build the app
yarn build

# alternatively, to create the packaged binary
yarn package

# or to build the packaged binary for a specific architecture
yarn package:latest:linux # or win for windows x64 / macos for mac x64
```

---

### Building for non-existing architectures

`docker-manifest` uses [pkg](https://github.com/zeit/pkg) for creating static executables. You may read through [pkg's documentation](https://github.com/zeit/pkg#targets) for more information about how to create static executables for your target os, architecture or node version.

---

### Using the `docker-manifest` Docker Image

> This is still work in progress. It's on the [roadmap](./roadmap) for the 1.0.0 stable release.
