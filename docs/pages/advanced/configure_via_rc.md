# Overriding default values used by `docker-manifest`

!!! warning
    This page is still a **work in progress** - and therefore might lack off the latest feature documentation or contain outdated / incorrect content.
    It's definitely **not recommended** to depend on this page's content in any way.
    Documentation which is **WIP** might change at any time.

## Using a `.docker-manifestrc`

`docker-manifest` uses [cosmiconf](https://www.npmjs.com/package/cosmiconfig) to load the default configuration.
This allows you to override any of the [default values]() with your own ones.  
In addition you can also extend the [configuration object]() with your own properties, which might be useful when [creating custom `docker-manifest` commands](./adding_additional_commands.md).

---

### Creating a custom configuration

Use one of the following ways to add your `docker-manifest` configuration:

- Add a `docker-manifest` property in your `package.json`
- Add a `.docker-manifestrc` file in the root of your project. (Content has to be JSON or YAML)
- Add a `.docker-manifestrc.json` file in the root of your project. (Content has to be valid JSON)
- Add a `.docker-manifestrc.yaml`, `.docker-manifestrc.yml` or `.docker-manifestrc.js` (Content has to be valid YAML/JavaScript)
- Add a `docker-manifest.config.js` which exports the configuration object.

!!! info
    When using any of the possibilities that allow you to write the config in `JavaScript`, make sure you're actually exporting the config object.

---

## Example Configurations

Below you can find some excerpts of how you can configure `docker-manifest`.

```json tab="Configure as property in package.json"
{
  "name": "your-node-project",
  "description": "An awesome node project",
  "version": "1.0.0",
  // ...
  "docker-manifest" : {
    // use GitLab's Container Registry instead of DockerHub
    "registry": "registry.gitlab.com",
    // set the default name of the manifest
    "manifestFile": "docker-manifest.yml",
    // ...
  }
}
```

```json tab="As .docker-manfiestrc with JSON Syntax"
{
  // use GitLab's Container Registry instead of DockerHub
  "registry": "registry.gitlab.com",
  // set the default name of the manifest
  "manifestFile": "docker-manifest.yml",
  // ...
}
```

```yaml tab="As .docker-manifestrc.yml using YAML Syntax"
---
# use GitLab's Containet Registry instead of DockerhHub
registry: registry.gitlab.com
# set the default name of the manifest
manifestFile: docker-manifest.yml
# ...
```

```javascript tab="As docker-manifest.config.js"
const config = {
  // use GitLab's Container Registry instead of DockerHub
  registry: "registry.gitlab.com",
  // set the default name of the manifest
  manifestFile: "docker-manifest.yml",
  // ...
}

// IMPORTANT: don't forget to export your config object!
module.exports = config
```

For more detailed examples you might want to check out the sample in the `examples/` folder.  
To get an overview of the default configuration object take a look at the [configuration](../configuration/) section. 

