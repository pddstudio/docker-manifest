# Extending `docker-manifest`

!!! warning
    This page is still a **work in progress** - and therefore might lack off the latest feature documentation or contain outdated / incorrect content.
    It's definitely **not recommended** to depend on this page's content in any way.
    Documentation which is **WIP** might change at any time.

> This page is still under construction.  
> Loading external commands is planned to be available upon the initial stable `1.0.0` release.  
> See also: [Roadmap](../roadmap/)
