# Getting Started

This page guides you through th the basuc `docker-manifest` installation and configuration process.  
It also covers how to pull / tag / publish docker images via `docker-manifest`.  

---

## Installing

Install `docker-manifest` via `npm` or `yarn`:

!!! info
    Using a NodeJS package manager is the _recommended_ but not the only way to get started..  
    The [Installing](../installing/) page contains more information about alternative ways to install `docker-manifest`.   
    

```bash tab="npm"

# installing via npm
npm install --global @pddstudio/docker-manifest

```

```bash tab="yarn"

# installing via yarn
yarn global add @pddstudio/docker-manifest

```

## Preparing the Working Directory

!!! note
    The dev environment is simply the directory where you want `docker-manifest` to operate in. (i.e the directory which contains a `manifest.yml`)  
     
For this guide we'll start setting everything up from scratch instead of [integrating `docker-manifest` into an existing project](../guides/integrate_into_existing_project/).  
To do so, we create a new direcoty and switch into it.  

```bash
# you can use any path of your choice as long as you
# have permissions to access the project directory
mkdir -p /tmp/docker-manifest-hello-world && cd $_
```

Once this is done we move further by creating a new `manifest.yml` _(or `manifest.yaml`)_ file with the following content: 

```yaml
# giving this project a name
name: Hello World from docker-manifest
# specifying which images are in this project
images:
  - base_image:
      # we use the official hello-world docker image
      image: hello-world
    dest_image:
      # prefixing the image so it can be pushed to the registry
      image: pddstudio/hello-world-from-docker-manifest
    build:
      # disable the build step because we're only (re)-tagging the original image
      required: false
      # push the destination image to the remote registry once all steps were processed
      publish: true
```

## Configuring

This allows you to modify the execution context (i.e the configuration) which is used by `docker-manifest` when it gets executed.

!!! note 
    Providing a configuration is optional.  
    If no config file is found `docker-manifest` will use the values of the default config instead.
    
    
!!! info
    To lean more about `docker-manifest`'s configuration you can head over to the [Configuration](../configuration/) page.


---

Finally pull, tag and republish the newly tagged image via `docker-manfiest`.  

!!! info "Information about publishing"
    `docker-manifest` will **neither touch nor write to** any of your related files.
    Keep this in mind - especially when re/publishing Images to rmeote Registries.
    To avoid any possible authentication errors you might want to run `docker login` right before calling `docker-manifest`.  

```bash
docker-manifest build --manifests ./manifest.yml
```

Once the Tasks finished execution uyou should have a new able to build them
    
---

## Reading further:

- [Configuration](../configuration/) to learn more about how to customize the way _how_ `docker-manifest` does its work
- [CLI Commands](../cli_commands/) to learn more about the different commands available
- [Manifest YAML](../manifest_yaml_reference/) to learn more about the `manifest.yml` file & how to configure it properly.
  
## Help & Support

In case you get stuck, have a feature request or want to submit a bug report, feel free to reach out to me.
Please make sure you've read && accppted the [Contribution Guidelines](../contributing/).
