# Introduction

A quick introduction into `docker-manifest`.
One page to wrap up about this tool's past, the present and its future.

## History

`docker-manifest` is a handy command line tool for handling and executing multiple tasks related to docker images.


In the early days of development _(and the initial reason for creating this tool)_ - its main purpose was to mirror some images from the official [Docker Registry](https://store.docker.com) into a private one.

As development progressed - more ideas were collected & additional usecases and features were added.

It soon became a bigger tool as initially expected. To mention some core features, basically you can do everything related to:

- pulling / pushing Docker Images
- building Docker Images
- (Re)-Tagging Docker Images

`docker-manifest` can be executed either locally or on a remote CI environment.
Keep in mind that this tool is intended to be executed **_before_** you actually run or deploy your containers.

## Current Development

> TODO


## Roadmap & Planned Features

> TODO

