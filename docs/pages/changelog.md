# Upgrading Instructions &  Release Changelog for `docker-manifest`

## Upgrading your installed `docker-manifest` version

If you installed `docker-manifest` via NodeJs package manager you can simply rerun the install command:


```bash tab="npm"

# installing via npm
npm install --global @pddstudio/docker-manifest

```

```bash tab="yarn"

# installing via yarn
yarn global add @pddstudio/docker-manifest

```

In case you used any of the alternative ways to install `docker-manifest` refer to the upgrade instructions as mentioned on the [Installing](./installing/) page.

---

## Changelog

!!! info
    Changelog will be tracked once `docker-manifest` reaches its initial stable `1.0.0` version.
