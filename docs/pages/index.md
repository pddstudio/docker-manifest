# Welcome 

This documentation covers everything related to the `docker-manifest` command line tool.  

!!! info
    This tool is sitll in early development stage.  
    Except if you're interested in [contributing](./contributing/) its highly recommended **not to use this tool** before the initial stable `1.0.0` release.

## Where to go from here

Below you can find some Links to get you started.
 
### New to `docker-manifest`?

- Start browsing this documentation by reading the [Introduction](./introduction/)
- Read the [Getting Started](./getting_started/) Guide

### About to use this tool?

- View the [list of available commands](./cli_commands/)
- Read how to [configure and override](./configuration/) the default configuration used by `docker-manifest`
- Explore the `manifest.yml` [YAML Reference](./manifest_yaml_reference/)

### Want to contribute?

- See the [Contribution Guide](./contributing/)
- Take a look at the [Roadmap](./roadmap/)

### Misc Links

- Browse the source code on [GitLab](https://gitlab.com/pddstudio/docker-manifest)
