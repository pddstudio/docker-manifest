# `docker-manifest` Configuration Object

!!! info
    The Configuration object is not related to the `manifest.yml`

## Default Configuration

By default, `docker-manifeat` uses the following configuration  

```javascript
module.exports = {
  dockerDefaults: {
    registry: '',
    tag: 'latest',
  }
}
```
