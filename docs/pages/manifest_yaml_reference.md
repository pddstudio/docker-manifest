# `manifest.yaml` File Reference

`docker-manifest` executes actions defined in the passed  `manifest.yml`.
Each project should contain only a single manifest definition as because `docker-manifest` is executing those tasks on a "project" point of view.  
See the fields explanation below to get more information about how to structure you `manifest.yml`  

## Supported Fields in `manifest.yml`

!!! tip "Including unknown properties in your manifest"
    If your manifest file contains unknown keys those are simply ignored.  
    This allows developers to merge the `manifest.yml` with any other kind of interface.  
    

The supported keys are documented below.  
For explanation and showcase samples this document assumes you're building a project based on NodeJS.

---

### `name: string`

!!! info "Required Parameter"
    This parameter is required and has to be present in your `manifest.yml`.

Defines the name for the target project.  
This is usually the name you provide in your project's `package.json` (assuming it's a node project)  

---

### `description: string`

!!! info ""
    This parameter is optional.  
    As of now this just refers to unimportant metadata which isn't handled by `docker-manifest` yet.  


Defines the description of the target project.  
This field usually contain the smae content as defiend in your `package.json`'s `description` field.

---

### `author: string`

!!! info ""
    This parameter is optional.  
    As of now this just refers to unimportant metadata which isn't handled by `docker-manifest` yet.  
    

Defines the author of the target project.  
When adding the author property you might ise the same value as declared in your `package.json`.

---

## Other additional afields

!!! info ""
    None of the fields mentioned in this section are processed or interpreted by `docker-manifest` in any way.


```yml
# optional additional fields
description: This is a simple description
author: Patrick Jung
# other optional values are:
homepage: "https://example.com/"
repository: "https://github.com/example/repo.git"
```

---

### `homepage: string`

This field could point to a project website or similar.  

---

### `repository: strint`

!!! info ""
    None of the fields mentioned in this section are processed or interpreted by `docker-manifest` in any way.


This field could point to a project repository.  

---

### `images: ImageSpec[]`

!!! info "Required Parameter"
    This parameter is required and has to be present in your `manifest.yml`.

An array of `ImageSpec` objects.  
Usually contains one or more `ImageSpec` references.  
In case no `ImageSpec` is present - `docker-manifeset` will skip its execution and finish.  
Please see the [ImageSpec](#image-spec) reference below for more details.  

---

## About the `ImageSpec` object

This object is intended to describe _how_ and _what_ kind of commands `docker-manifest` should execute.  
An **image spec**ification consists of three main building blocks:

- The original or target image to work with
- The destination or output image
- (Optional) An object which contains some configuration related to building & publishing images

---

### `base_image: DockerImage`

Defines the remote image which this task should work with.  
See the [DockerImage]() reference below for more details.

!!! tip
    When writing your own `Dockerfile` this is usually the `base` image name your on which your docker image builds upon:  

    ```sh
    FROM ${image}
    ```

---

Comparing `DockerImage` with standart `docker` commands:

```bash tab="Bash"
# pull the official hello-world docker image from the
# official docker registry:

docker pull hello-world

```

```yaml tab="manifest.yml"
# ... other declarations
images:
  # use the official hello-world image as base
  - base_image:
    image: hello-world
```

---

> TODO: To be continued...
