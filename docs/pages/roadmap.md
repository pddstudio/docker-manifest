# Roadmap for `docker-manifest`

The list below contains all tasks & planned features that need to be added before `docker-manifest`'s initial stable version.

## Pending tasks until stable `1.0.0` release:

[ ] More detailed documentation  
[ ] Resolve all TODOs  
[ ] Automated build & deployment  
[ ] Create docker image for `docker-manifest`  

