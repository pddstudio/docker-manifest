# Samples for `docker-manifest` & `manifest.yml`

This section covers the following section:

## `hello-world-from-docker-manifest`

Contains the minimal setup reqiured for a working `manifest.yml`.

### Sections covered:

- pull the official hello-world image from docker
- re-tag the hello-world image to a namespace which the docker daemon has access to
- re-publish the image using the specified namespace.

## `custom-webserver-using-dockerfile`

Sample setup how to build a custom webserver image from a `Dockerfile`.

 ### Sections covered:

- pull the latest `nginx:alpine` image
- build a custom image using a `Dockerfile`
- publish the custom image to the specified namespace/repository
